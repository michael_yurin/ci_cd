const fs = require('fs');
const path = require('path');
const assert = require('assert');
const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');

let driver;
const browser = process.env.BROWSER || 'chrome';

describe(`Autotest`, function () {
  this.timeout(30000);

  before(function () {
    driver = new webdriver.Builder()
      .forBrowser(browser)
      .setChromeOptions(
        new chrome.Options()
          .headless()
          .addArguments('--no-sandbox')
      )
      .build();
    // driver.manage().window().setSize(2000, 1200);
  });
  after(async function () {
    const data = await driver.takeScreenshot();
    const filename = path.resolve('artifacts', 'screenshot.png');
    fs.writeFileSync(filename, data.replace(/^data:image\/png;base64,/, ''), 'base64');
    await driver.quit();
  });

  it('Open main page', async function () {
    await driver.get('https://ci-cd-test-server.herokuapp.com/');
  });
  it('Check text on the page', async function () {
    const expected = 'I love cats!';
    const actual = await driver.findElement(webdriver.By.xpath('.//h2')).getText();
    assert.strictEqual(actual, expected, `Can't find "${expected}" on the page :(`);
  });
});
